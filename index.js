console.log("Hello World");

let number = 2;
let getCube = number **3;

console.log(`THe Cube of ${number} is ${getCube}`);

let address = ["258", "Washington Ave", "NW", "California", "90011"];
const [streeNumber, streetName, direction, country, postalCode] = address;
console.log(`I live at ${streeNumber} ${streetName} ${direction} ${country} ${postalCode}`);

let animal = {
	name: "Lolong",
	habitat: "saltwater",
	type: "crocodile",
	weight: 1075,
	feet: 20,
	inches: 3
};

const {name, habitat, type, weight, feet, inches} = animal;
console.log(`${name} was a ${habitat} ${type}. He weighed at ${weight} kgs with a measurement of ${feet} ft ${inches} in.`);

let myArray = [1, 2, 3, 4, 5];

myArray.forEach((number) => console.log(number));

let reduceNumber = myArray.reduce((x, y) => {
	return x + y; 
});

console.log(reduceNumber);